const express = require("express");
const db = require("./database/connection.js");
const app = express();
// import cors from "cors";

// app.use(cors());

//middleware
app.use(express.json());

app.get("/api", (req, res) => {
  res.send("hello world");
});

app.get("/api/cats", (req, res) => {
  db.query("SELECT * FROM cats", function (error, results, fields) {
    if (error) throw error;
    return res.status(200).send(results);
  });
});

//one cat
app.get("/api/cats/:catid", (req, res) => {
  db.query(
    `SELECT * FROM cats WHERE id=${req.params.catid}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

app.post("/api/cats", (req, res) => {
  db.query(
    "INSERT INTO cats (name, image) VALUES (?, ?)",
    [req.body.name, req.body.image],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

app.delete("/api/cats/:id", (req, res) => {
  db.query(
    `DELETE FROM cats WHERE id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

app.put("/api/cats/:id", (req, res) => {
  const { name, image } = req.body;
  db.query(
    `UPDATE cats SET name="${name}", image="${image}" WHERE id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

const PORT = 3001;
app.listen(PORT, () => console.log(`Server started on port 3001...`));
